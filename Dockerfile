FROM eclipse-temurin:17 as builder

WORKDIR application

COPY .mvn ./.mvn
COPY pom.xml mvnw ./
RUN chmod +x ./mvnw
RUN ./mvnw dependency:go-offline
COPY src src
RUN ./mvnw -DskipTests package

# Make the executable image
FROM eclipse-temurin:17-jre-alpine

# Get the app
WORKDIR application
COPY --from=builder application/target/*.jar ./app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]
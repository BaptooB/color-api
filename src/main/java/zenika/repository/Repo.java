package zenika.repository;

import org.springframework.data.repository.CrudRepository;
import zenika.entity.ColorEntity;

import java.util.Optional;

public interface Repo extends CrudRepository<ColorEntity, String> {
    Optional<ColorEntity> findByName(String name);
}

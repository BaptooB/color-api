package zenika.entity;




import javax.persistence.*;
import java.awt.*;

@Table(name = "colors")
@Entity
@Access(AccessType.FIELD)
public class ColorEntity {

    @Id
    String uuid;
    String name;

    protected ColorEntity(){
        // FOR JPA
    }
    public ColorEntity(String uuid, String name){
        this.uuid = uuid;
        this.name = name;
    }
}

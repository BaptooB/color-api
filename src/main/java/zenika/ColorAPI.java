package zenika;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ColorAPI {

    public static void main(String[] args){
        SpringApplication.run(ColorAPI.class, args);
    }
}

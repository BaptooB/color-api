package zenika.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import zenika.DTO.RgbDTO;
import zenika.entity.ColorEntity;
import zenika.repository.Repo;


import javax.annotation.PostConstruct;
import java.util.Random;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*")
public class ColorController{

    @Autowired
    private Random myRandom;

    @Autowired
    Repo myRepo;

    @PostConstruct
    private void initDB(){
        this.myRepo.save(new ColorEntity(UUID.randomUUID().toString(), "RED"));
        this.myRepo.save(new ColorEntity(UUID.randomUUID().toString(), "GREEN"));
        this.myRepo.save(new ColorEntity(UUID.randomUUID().toString(), "BLUE"));
        this.myRepo.save(new ColorEntity(UUID.randomUUID().toString(), "YELLOW"));
    }


    @GetMapping("/colors/random")
    public ResponseEntity<RgbDTO> getRandomColor(){
        int r = this.myRandom.nextInt(256);
        int g = this.myRandom.nextInt(256);
        int b = this.myRandom.nextInt(256);
        RgbDTO randomColor = new RgbDTO(r, g, b);

        return ResponseEntity.ok(randomColor);
    }

    @GetMapping("/colors/{name}")
    public ResponseEntity<ColorEntity> getColor(@PathVariable String name){
        ColorEntity color = this.myRepo.findByName(name).orElse(null);

        if (color != null) {
            return ResponseEntity.ok(color);

        }
        return ResponseEntity.badRequest().build();

    }

}